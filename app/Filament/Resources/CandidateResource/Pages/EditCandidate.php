<?php

namespace App\Filament\Resources\CandidateResource\Pages;

use App\Filament\Resources\CandidateResource;
use App\Models\User;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditCandidate extends EditRecord
{
    protected static string $resource = CandidateResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }

    protected function mutateFormDataBeforeFill(array $data): array
    {
        $user = User::find($data['user_id']);
        $data['name'] = $user->name;
        $data['email'] = $user->email;
        $data['slug'] = $user->slug;

        return $data;
    }

    protected function mutateFormDataBeforeSave(array $data): array
    {
        $user = User::find(2);
//        $user = User::find($data['user_id']);

        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->slug = $data['slug'];

        return $data;
    }
}
